<?php 

interface Logger {
	public function execute($message);
}

/**
 * 
 */
class LogToDatabase implements Logger
{
	public function execute($message) {
		var_dump("This will log to DATABASE : ".$message );
	}	

}


/**
 * 
 */
class LogToFile implements Logger
{
	
	public function execute($message) {
		var_dump("This will log to FILE : ".$message );
	}
}


class UserController {
	protected  $logger;
	function __construct(Logger $logger) {
		$this->logger = $logger;
	}
	public function show($name) {
		$this->logger->execute($name);
	}
}

$controller = new UserController(new LogToFile);
$controller->show("Tamjid Sarkar");
