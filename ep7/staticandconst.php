<?php

echo "Static and constants.\n";

class Math {
    public static function add(...$num) {
        return array_sum($num);
    }
}

$sum = Math::add(1,2,3);

$str = "foobar";
echo $sum."\n";