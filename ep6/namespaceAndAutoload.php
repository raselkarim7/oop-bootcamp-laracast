<?php
echo "Naming Spaces and Autoloading..";
echo "\n";

use Acme\Users\Person;
use Acme\Staff;
use Acme\Business;


$personOne = new Person('Tamjid Haque');
$staff = new Staff([$personOne]);
$laracast = new Business($staff);
$personTwo = new Person('Rasel Karim');
$laracast->hire($personTwo);
$totalStaffs = $laracast->getStaffMembers();
var_dump($totalStaffs);

?>