<?php
namespace Acme;
use Acme\Users\Person;

class Business {
    protected $staff;
    public function __construct(Staff $staff)
    {
        $this->staff = $staff;
    }

    function hire(Person $person) {
        $this->staff->addMember($person);
    }

    function getStaffMembers() {
        return $this->staff->members();
    }
}

?>