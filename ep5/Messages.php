<?php
    echo "Hellooooooo Worldddddddddd";
    echo "\n";  
    class Person {
        public $name;
        function __construct($name) {
            $this->name = $name;
        }
    }
    

    class Staff {
        protected $members = [];
        public function __construct($members = [])
        {
            $this->members = $members;
        }

        function addMember($member) {
            $this->members[] = $member;
        }

        public function members() {
            return $this->members;
        }

    }


    class Business {
        protected $staff;
        public function __construct(Staff $staff)
        {
            $this->staff = $staff;
        }

        function hire($person) {
            $this->staff->addMember($person);
        }

        function getStaffMembers() {
            return $this->staff->members();
        }


    }

    $person = new Person('Tamjid Haque');
    $staff = new Staff([$person]);
    $laracast = new Business($staff);
    $laracast->hire('Rasel Karim');
    $total = $laracast->getStaffMembers();
    var_dump($laracast);

?>